﻿using LoadTesterEngine;
using System;
using System.IO;

namespace LoadTesterConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                var instructions = File.ReadAllText("Instructions.txt");
                Console.Write(instructions);
                return;
            }

            string scenarioJson = null;

            if (args.Length == 1)
            {
                if (File.Exists(args[0]))
                {
                    scenarioJson = File.ReadAllText(args[0]);
                }
            }

            using (Engine engine = new Engine(scenarioJson))
            {
                engine.Initialize();

                engine.Execute();

                Console.ReadKey();

                engine.Abort();
            }
        }
    }
}
