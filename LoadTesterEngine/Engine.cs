﻿using LoadTesterEngine.Work;
using System;
using System.Collections.Generic;
using System.Threading;

namespace LoadTesterEngine
{
    public class Engine : IDisposable
    {
        private List<Thread> threads = null;

        private Random random { get; set; }
        private Scenario scenario { get; set; }

        private System.Timers.Timer timer { get; set; }

        private bool IsExecuting = false;

        public Engine(string scenarioJson)
        {
            threads = new List<Thread>();
            random = new Random(DateTime.Now.Millisecond);
            scenario = Newtonsoft.Json.JsonConvert.DeserializeObject<Scenario>(scenarioJson);
            timer = new System.Timers.Timer();
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Abort();
        }

        public void Initialize()
        {
            for (int i = 0; i < scenario.NumberOfProcesses; i++)
            {
                foreach (var scenarioRequest in scenario.ScenarioRequests)
                {
                    Thread thread = new Thread(() => new Process(random, scenarioRequest).Execute());

                    threads.Add(thread);
                }
            }
            timer.Interval = (double)scenario.Duration * 1000;
        }

        public void Execute()
        {
            this.IsExecuting = true;

            foreach (Thread thread in threads)
            {
                thread.Start();
            }
            timer.Start();
        }

        public void Abort()
        {
            if (!IsExecuting)
            {
                return;
            }
            IsExecuting = false;

            timer.Stop();
            foreach (Thread thread in threads)
            {
                thread.Interrupt();
            }
        }

        public void Dispose()
        {
            if (timer != null)
            {
                timer.Dispose();
            }
        }
    }
}
