﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoadTesterEngine.Work
{
    internal class Scenario
    {
        public int NumberOfProcesses { get; set; }
        public int Duration { get; set; }

        public List<ScenarioRequest> ScenarioRequests { get; set; }
    }
}
