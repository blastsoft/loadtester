﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LoadTesterEngine.Work
{
    internal class ScenarioRequest
    {
        public string Url { get; set; }
        public string Method { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public int MinDelay { get; set; }
        public int MaxDelay { get; set; }
    }
}
