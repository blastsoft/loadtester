﻿using LoadTesterEngine.Work;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;

namespace LoadTesterEngine
{
    internal class Process
    {
        private Random random { get; set; }
        private ScenarioRequest scenarioRequest { get; set; }

        public Process(Random random, ScenarioRequest scenarioRequest)
        {
            this.random = random;
            this.scenarioRequest = scenarioRequest;
        }

        public async void Execute()
        {
            Stopwatch stopWatch = null;
            try
            {
                Thread.Sleep(random.Next(scenarioRequest.MinDelay, scenarioRequest.MaxDelay));
                while (true)
                {
                    using (var client = new HttpClient())
                    {
                        Uri uri = new Uri(scenarioRequest.Url);
                        client.BaseAddress = uri;
                        HttpResponseMessage response = null;
                        Console.WriteLine(String.Format("<BEGIN>  ThreadID : {0} | URL : {1} | Method : {2}", Thread.CurrentThread.ManagedThreadId.ToString(), scenarioRequest.Url, scenarioRequest.Method));
                        stopWatch = Stopwatch.StartNew();
                        if (scenarioRequest.Method == HttpMethod.Get.Method)
                        {
                            response = await client.GetAsync(uri);
                        }
                        stopWatch.Stop();
                        Console.WriteLine(String.Format("<FINISH> ThreadID : {0} | URL : {1} | Method : {2} | Response Code : {3} | Response Time : {4}", Thread.CurrentThread.ManagedThreadId.ToString(), scenarioRequest.Url, scenarioRequest.Method, response.StatusCode.ToString(), stopWatch.ElapsedMilliseconds.ToString()));
                    }


                    Thread.Sleep(random.Next(scenarioRequest.MinDelay, scenarioRequest.MaxDelay));
                }
            }
            catch (ThreadInterruptedException)
            {
                Console.WriteLine("Wątek zakończony ID : " + Thread.CurrentThread.ManagedThreadId.ToString());
            }
        }
    }
}
