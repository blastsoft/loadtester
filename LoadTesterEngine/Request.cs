﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LoadTesterEngine
{
    internal class Request
    {
        public string Url { get; set; }

        public HttpMethod Method { get; set; }
    }
}
